import $ from 'jquery';

$(function () {
	// $('input.datepicker').datepicker();
});

export function sum(items) {
	return items.reduce((acc, item) => acc + parseInt((item.amount ? item.amount : 0), 10) || 0, 0);
}

export function getCurMonth() {
	return new Date().getMonth() + 1;
}

export function getCurYear() {
	return new Date().getFullYear();
}

export function getYearMonth() {
	return {
		year: getCurYear(),
		month: getCurMonth()
	};
}

export function getMonthData(data) {
	const date = getYearMonth();
	const month = date.month;
	const year = date.year;
	const curYearData = data.filter(node => node.year === +year)[0];

	return curYearData.months.filter(
		node => node.month.number === month
	)[0];
}
