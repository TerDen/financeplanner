import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';

import {Provider} from 'react-redux';
import ErrorBoundary from './js/components/ErrorBoundary/ErrorBoundary';

import Finance from './js/containers/Finance/Finance.js';
// импортируем store
import store from './js/store/configureStore';
import './index.scss';

import './common.js';
import './assets/libs/bootstrap/dist/css/bootstrap.min.css';
import './assets/libs/bootstrap/dist/css/bootstrap-theme.min.css';
import './assets/libs/animate.css/animate.min.css';

ReactDOM.render(
	<Provider store={store}>
		<ErrorBoundary>
			<Finance/>
		</ErrorBoundary>
	</Provider>,
	document.getElementById('main')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister();
