import paymentsData from './data/dataJson';
import {getYearMonth} from '../../common';

export default function nav_reducer(state, action) {
	const date = getYearMonth();
	const year = date.year;
	const monthsData = [];
	const yearData = paymentsData.filter(node => node.year === +year)[0];

	yearData.months.forEach(node => monthsData.push(node.month));

	if (!state) {
		state = {
			monthsData,
			year
		};
	}

	switch (action.type) {
	default:
		return state;
	}
}
