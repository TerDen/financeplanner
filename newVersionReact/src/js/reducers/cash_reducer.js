import paymentsData from './data/dataJson';
import AT from '../constants/constants';
import {sum, getMonthData} from '../../common';

import {addPayment} from '../actions/addPayment';
import {editItemSum} from '../actions/editItemSum';
import {saveItemDescription} from '../actions/saveItemDescription';
import {deleteItem} from '../actions/deleteItem';

export default function cash_reducer (state, action) {
	const curMonthData = getMonthData(paymentsData);
	const {cashDescription: cashList} = curMonthData.description.cash;

	if (!state) {
		state = {
			list: cashList,
			sum: sum(cashList)
		};
	}

	switch (action.type) {
	case AT.ADD_CASH: {
		return addPayment(state, action);
	}

	case AT.EDIT_ITEM_SUM: {
		return editItemSum(state, action);
	}

	case AT.SAVE_ITEM_DESC: {
		return saveItemDescription(state, action);
	}

	case AT.DELETE_ITEM: {
		return deleteItem(state, action);
	}

	default:
		return state;
	}
}
