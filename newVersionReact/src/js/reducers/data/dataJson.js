const paymentsData = [
	{
		year: 2018,
		months: [
			{
				month: {
					number: 10,
					name: 'October',
					status: null
				},
				description: {
					'main': {
						'date': '',
						'paymentsDescription': [
							{
								'id': 1,
								'text': 'Аренда',
								'amount': 550,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': 'oops'
							},
							{
								'id': 2,
								'text': 'Квартплата',
								'amount': 100,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 3,
								'text': 'Питание',
								'amount': 550,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 4,
								'text': 'Бензин',
								'amount': 100,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 5,
								'text': 'Мобильная связь',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 6,
								'text': 'Проездной',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 7,
								'text': 'Спорт',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							}
						],
						'totalSum': null
					},
					'second': {
						'date': '',
						'paymentsDescription': [
							{
								'id': 8,
								'text': 'Аренда',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 9,
								'text': 'Квартплата',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 10,
								'text': 'Питание',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 11,
								'text': 'Бензин',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 12,
								'text': 'Мобильная связь',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 13,
								'text': 'Проездной',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 14,
								'text': 'Спорт',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							}
						],
						'totalSum': null
					},
					'cash': {
						'cashDescription': [
							{
								'id': 15,
								'text': 'OTS',
								'amount': 1150,
								'itemType': 'cashItem',
								'description': 'OnTravelSolution',
								'receive_date': {
									'main': '',
									'second': ''
								}
							},
							{
								'id': 16,
								'text': 'Белросстрах',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Белросстрах',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 17,
								'text': 'ТАСК',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Таск',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 18,
								'text': 'Белгосстрах',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Белгосстрах',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 19,
								'text': 'Нина Петровна',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Нина Петровна',
								'receive_date': {
									'main': '',
									'second': ''
								}
							},
							{
								'id': 20,
								'text': 'Детские',
								'amount': 390,
								'itemType': 'cashItem',
								'description': 'Детские',
								'receive_date': {
									'main': '',
									'second': ''
								}
							}
						],
						'totalSum': null
					}
				}
			},
			{
				month: {
					number: 11,
					name: 'November',
					status: 1
				},
				description: {
					'main': {
						'date': '',
						'paymentsDescription': [
							{
								'id': 1,
								'text': 'Аренда',
								'amount': 550,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': 'oops'
							},
							{
								'id': 2,
								'text': 'Квартплата',
								'amount': 100,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 3,
								'text': 'Питание',
								'amount': 550,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 4,
								'text': 'Бензин',
								'amount': 100,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 5,
								'text': 'Мобильная связь',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 6,
								'text': 'Проездной',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 7,
								'text': 'Спорт',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							}
						],
						'totalSum': null
					},
					'second': {
						'date': '',
						'paymentsDescription': [
							{
								'id': 8,
								'text': 'Аренда',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 9,
								'text': 'Квартплата',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 10,
								'text': 'Питание',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 11,
								'text': 'Бензин',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 12,
								'text': 'Мобильная связь',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 13,
								'text': 'Проездной',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 14,
								'text': 'Спорт',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							}
						],
						'totalSum': null
					},
					'cash': {
						'cashDescription': [
							{
								'id': 15,
								'text': 'OTS',
								'amount': 1150,
								'itemType': 'cashItem',
								'description': 'OnTravelSolution',
								'receive_date': {
									'main': '',
									'second': ''
								}
							},
							{
								'id': 16,
								'text': 'Белросстрах',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Белросстрах',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 17,
								'text': 'ТАСК',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Таск',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 18,
								'text': 'Белгосстрах',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Белгосстрах',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 19,
								'text': 'Нина Петровна',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Нина Петровна',
								'receive_date': {
									'main': '',
									'second': ''
								}
							},
							{
								'id': 20,
								'text': 'Детские',
								'amount': 390,
								'itemType': 'cashItem',
								'description': 'Детские',
								'receive_date': {
									'main': '',
									'second': ''
								}
							}
						],
						'totalSum': null
					}
				}
			},
			{
				month: {
					number: 1,
					name: 'December',
					status: null
				},
				description: {
					'main': {
						'date': '',
						'paymentsDescription': [
							{
								'id': 1,
								'text': 'Аренда',
								'amount': 550,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': 'oops'
							},
							{
								'id': 2,
								'text': 'Квартплата',
								'amount': 100,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 3,
								'text': 'Питание',
								'amount': 550,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 4,
								'text': 'Бензин',
								'amount': 100,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 5,
								'text': 'Мобильная связь',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 6,
								'text': 'Проездной',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 7,
								'text': 'Спорт',
								'amount': null,
								'itemType': 'mainPayment',
								'obligatory': true,
								'description': ''
							}
						],
						'totalSum': null
					},
					'second': {
						'date': '',
						'paymentsDescription': [
							{
								'id': 8,
								'text': 'Аренда',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 9,
								'text': 'Квартплата',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 10,
								'text': 'Питание',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 11,
								'text': 'Бензин',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 12,
								'text': 'Мобильная связь',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 13,
								'text': 'Проездной',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							},
							{
								'id': 14,
								'text': 'Спорт',
								'amount': null,
								'itemType': 'secondPayment',
								'obligatory': true,
								'description': ''
							}
						],
						'totalSum': null
					},
					'cash': {
						'cashDescription': [
							{
								'id': 15,
								'text': 'OTS',
								'amount': 1150,
								'itemType': 'cashItem',
								'description': 'OnTravelSolution',
								'receive_date': {
									'main': '',
									'second': ''
								}
							},
							{
								'id': 16,
								'text': 'Белросстрах',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Белросстрах',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 17,
								'text': 'ТАСК',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Таск',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 18,
								'text': 'Белгосстрах',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Белгосстрах',
								'receive_date': {
									'main': '',
									'second': ''
								}

							},
							{
								'id': 19,
								'text': 'Нина Петровна',
								'amount': '',
								'itemType': 'cashItem',
								'description': 'Нина Петровна',
								'receive_date': {
									'main': '',
									'second': ''
								}
							},
							{
								'id': 20,
								'text': 'Детские',
								'amount': 390,
								'itemType': 'cashItem',
								'description': 'Детские',
								'receive_date': {
									'main': '',
									'second': ''
								}
							}
						],
						'totalSum': null
					}
				}
			}
		]
	}
];

export default paymentsData;
