import paymentsData from './data/dataJson';
import AT from '../constants/constants';
import {sum, getMonthData} from '../../common';

import {addPayment} from '../actions/addPayment';
import {editItemSum} from '../actions/editItemSum';
import {saveItemDescription} from '../actions/saveItemDescription';
import {deleteItem} from '../actions/deleteItem';

export default function second_reducer (state, action) {
	const curMonthData = getMonthData(paymentsData);
	const {paymentsDescription: secondPayment} = curMonthData.description.second;

	if (!state) {
		state = {
			list: secondPayment,
			sum: sum(secondPayment)
		};
	}

	switch (action.type) {
	case AT.ADD_PAYMENT_SECOND: {
		return addPayment(state, action);
	}

	case AT.EDIT_ITEM_SUM: {
		return editItemSum(state, action);
	}

	case AT.SAVE_ITEM_DESC: {
		return saveItemDescription(state, action);
	}

	case AT.DELETE_ITEM: {
		return deleteItem(state, action);
	}

	case AT.COPY_PAYMENTS: {
		const copyMain = [...action.newPayments],
			copySecond = [...state.list];

		copyMain.forEach((node, i) => {
			if (copySecond[i]) {
				copySecond[i].amount = node.amount;
				copySecond[i].description = node.description;
			}
		});

		return Object.assign({}, state, {
			list: copySecond,
			sum: sum(copySecond)
		});
	}

	default:
		return state;
	}
}
