import {combineReducers} from 'redux';

import nav_reducer from './nav_reducer';
import main_reducer from './main_reducer';
import second_reducer from './second_reducer';
import cash_reducer from './cash_reducer';


export default combineReducers({
	nav: nav_reducer,
	main: main_reducer,
	second: second_reducer,
	cash: cash_reducer
});
