import paymentsData from './data/dataJson';
import AT from '../constants/constants';
import {sum, getMonthData} from '../../common';

import {addPayment} from '../actions/addPayment';
import {editItemSum} from '../actions/editItemSum';
import {saveItemDescription} from '../actions/saveItemDescription';
import {deleteItem} from '../actions/deleteItem';

export default function main_reducer (state, action) {
	const curMonthData = getMonthData(paymentsData);
	const {paymentsDescription: mainPayment} = curMonthData.description.main;

	if (!state) {
		state = {
			list: mainPayment,
			sum: sum(mainPayment)
		};
	}

	switch (action.type) {
	case AT.ADD_PAYMENT_MAIN: {
		return addPayment(state, action);
	}

	case AT.EDIT_ITEM_SUM: {
		return editItemSum(state, action);
	}

	case AT.SAVE_ITEM_DESC: {
		return saveItemDescription(state, action);
	}

	case AT.DELETE_ITEM: {
		return deleteItem(state, action);
	}

	default:
		return state;
	}
}
