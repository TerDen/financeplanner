import C from '../constants/constants';

export const editItemSum = payload => ({
	type: C.EDIT_ITEM_SUM,
	id: payload.id,
	sum: payload.sum,
	itemType: payload.type
});

export const calculateTotal = () => ({
	type: C.TOTAL_SUM
});

export const copyPayments = newPayments => ({
	type: C.COPY_PAYMENTS,
	newPayments
});

export const saveItemDesc = payload => ({
	type: C.SAVE_ITEM_DESC,
	id: payload.id.toString(),
	paymentDesc: payload.paymentDesc
});

export const deleteItem = payload => ({
	type: C.DELETE_ITEM,
	id: payload.id.toString(),
	itemType: payload.itemType
});

export const addPaymentMain = newItem => ({
	type: C.ADD_PAYMENT_MAIN,
	newItem
});

export const addPaymentSecond = newItem => ({
	type: C.ADD_PAYMENT_SECOND,
	newItem
});

export const addCash = newItem => ({
	type: C.ADD_CASH,
	newItem
});
