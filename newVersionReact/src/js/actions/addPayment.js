import {sum} from '../../common';

export const addPayment = (state, action) => {
	const newPayment = action.newItem;

	const newPayments = [
		...state.list,
		newPayment
	];

	return Object.assign({}, state, {
		list: newPayments,
		sum: sum(newPayments)
	});
};
