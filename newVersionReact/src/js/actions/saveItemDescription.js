export const saveItemDescription = (state, action) => {
	const id = action.id,
		saveDesc = action.paymentDesc.desc,
		newState = [...state.list];

	newState.forEach(node => {
		if (node.id.toString() === id) return node.description = saveDesc;
	});

	return Object.assign({}, state, {
		list: newState
	});
};
