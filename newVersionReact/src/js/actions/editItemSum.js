import {sum} from '../../common';

export const editItemSum = (state, action) => {
	const id = action.id,
		newSum = action.sum,
		newState = [...state.list];

	newState.forEach(node => {
		if (node.id.toString() === id) return (node.amount = newSum);
	});

	return Object.assign({}, state, {
		list: newState,
		sum: sum(newState)
	});
};
