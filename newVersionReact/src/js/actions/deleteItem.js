import {sum} from '../../common';

export const deleteItem = (state, action) => {
	const id = action.id,
		newState = [...state.list];

	newState.some((node, i) => {
		if (node.id.toString() !== id) return false;
		return newState.splice(i, 1);
	});

	return Object.assign({}, state, {
		list: newState,
		sum: sum(newState)
	});
};
