import React from 'react';
import PropTypes from 'prop-types';
import styles from './ItemList.module.scss';

import TotalSum from '../TotalSum/TotalSum.js';

const ItemsList = ({total_name, totalSumVal, children}) => {
	return (
		<div>
			<ul className={styles.list}>{children}</ul>

			<TotalSum total_name={total_name} totalSumVal={totalSumVal}/>
		</div>
	);
};

TotalSum.propTypes = {
	total_name: PropTypes.string,
	totalSumVal: PropTypes.number
};

export default ItemsList;
