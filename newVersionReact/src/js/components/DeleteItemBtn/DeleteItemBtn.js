import React from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';
import styles from './DeleteItemBtn.module.scss';

export default class DeleteItemBtn extends React.Component {
	constructor() {
		super();

		this.state = {
			attention: false
		};

		this.btnChangeStatusHandler = this.btnChangeStatusHandler.bind(this);
	}

	btnChangeStatusHandler() {
		const {attention} = this.state,
			{deleteItem} = this.props;

		this.setState({
			attention: !attention
		});

		if (attention) deleteItem();
	}

	render() {
		return (
			<span
				className={`btn ${styles.deleteItem} ${
					this.state.attention ? 'btn-warning' : 'btn-default'
				}`}
				onClick={this.btnChangeStatusHandler}
			>
				<FontAwesomeIcon icon='trash'/>
			</span>
		);
	}
}

DeleteItemBtn.propTypes = {
	deleteItem: PropTypes.func
};
