import React, {Component} from 'react';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import ItemsList from '../ItemsList/ItemsList.js';
import AddItemBtn from '../AddItemBtn/AddItemBtn.js';
import CopySumBtn from '../CopySumBtn/CopySumBtn.js';
import Payment from '../Payment/Payment.js';
import {getCurMonth, getCurYear} from '../../../common';

import styles from './scss/Month.module.scss';
import PaymentModal from '../../containers/PaymentModal/PaymentModal';

export default class Month extends Component {
	constructor(props) {
		super(props);

		this.state = {
			defaultDate: '',
			currentModalType: '',
			modalOpen: false
		};
		this.handlerCopyPaymentsSumBtn = this.handlerCopyPaymentsSumBtn.bind(this);
		this.handlerSaveItemDescBtn = this.handlerSaveItemDescBtn.bind(this);
		this.handlerBtnDeleteItem = this.handlerBtnDeleteItem.bind(this);
		this.handlerModal = this.handlerModal.bind(this);
		this.toggleModal = this.toggleModal.bind(this);
	}

	static getDate() {
		const fullDate = new Date();
		return fullDate.toLocaleDateString();
	}

	setDefaultDate() {
		const toDay = Month.getDate();
		this.setState({
			defaultDate: toDay
		});
	}

	componentDidMount() {
		this.setDefaultDate();
		this.props.calculateTotal();
	}

	handlerCopyPaymentsSumBtn() {
		this.props.handlerClickCopyBtn(this.props.mainPayments);
		this.props.calculateTotal();
	}

	handlerSaveItemDescBtn(newDesc, id) {
		this.props.handlerSaveItemDescBtn(newDesc, id);
	}

	handlerBtnDeleteItem(id, type) {
		this.props.handlerBtnDeleteItem(id, type);
		this.props.calculateTotal();
	}

	handlerModal(event) {
		const modalType = event.target.dataset.modaltype;
		modalType && this.setState({currentModalType: modalType});
		this.toggleModal();
	}

	toggleModal() {
		this.setState(state => {
			return {modalOpen: !state.modalOpen};
		});
	}

	render() {
		const months = [
			'Январь',
			'Февраль',
			'Март',
			'Апрель',
			'Май',
			'Июнь',
			'Июль',
			'Август',
			'Сентябрь',
			'Октябрь',
			'Ноябрь',
			'Декабрь'
		];

		const payments_main = this.props.mainPayments.map((payment, i) => (
			<Payment
				key={i}
				data={payment}
				onClickBtnSave={this.handlerSaveItemDescBtn}
				onChangePaymentSum={this.props.handlerEditItemSum}
				deleteItem={this.handlerBtnDeleteItem}
			/>
		));

		const payments_second = this.props.secondPayments.map((payment, i) => (
			<Payment
				key={i}
				data={payment}
				onClickBtnSave={this.handlerSaveItemDescBtn}
				onChangePaymentSum={this.props.handlerEditItemSum}
				deleteItem={this.handlerBtnDeleteItem}
			/>
		));

		const cash_list = this.props.cashList.map((cash, i) => (
			<Payment
				key={i}
				data={cash}
				onClickBtnSave={this.handlerSaveItemDescBtn}
				onChangePaymentSum={this.props.handlerEditItemSum}
				deleteItem={this.handlerBtnDeleteItem}
			/>
		));

		return (
			<div className={styles.Month}>
				<h2 className={styles.monthName}>{months[getCurMonth()]}</h2>

				<div className={styles.mainWrapper}>
					{/* Основной расчет*/}
					<div
						className={`${styles.mainPayments} ${styles.paymentColumn} _width33`}
						data-payment='main'
					>
						<h3>Основной расчет</h3>
						<div className='_margin-top-30'>
							<label className='_relative _fullWidth'>
								<span className={styles.calendar}>
									<FontAwesomeIcon icon='calendar-alt'/>
								</span>
								<input
									className={`${styles.datepicker} datepicker`}
									type='text'
									data-type='datePicker'
									value={
										'01.' + (getCurMonth() + 1) + '.' + getCurYear()
									}
									disabled
								/>
							</label>
							<ItemsList
								children={payments_main}
								total_name='Итого расходов:'
								totalSumVal={this.props.mainSum}
							/>
						</div>

						<AddItemBtn
							type='payment'
							BtnName='расходов'
							modalHandler={this.handlerModal}/>
					</div>

					{/* Промежуточный расчет*/}
					<div className={`${styles.paymentColumn} _width33`} data-payment='main'>
						<h3>Промежуточный расчет</h3>
						<div className='_margin-top-30'>
							<label className='_relative _fullWidth'>
								<span className={styles.calendar}>
									<FontAwesomeIcon icon='calendar-alt'/>
								</span>
								<input
									className={`${styles.datepicker} datepicker`}
									type='text'
									data-type='datePicker'
									defaultValue={this.state.defaultDate}
								/>
							</label>
							<ItemsList
								children={payments_second}
								total_name='Итого расходов:'
								totalSumVal={this.props.secondSum}
							/>
						</div>
						<CopySumBtn onClickCopyBtn={this.handlerCopyPaymentsSumBtn}/>
					</div>

					<div className={`${styles.moneyDeltaBlock} _width33`}>
						<div className={styles.monthCash}>
							<h3 className='text-center'>Доходы</h3>

							<ItemsList
								children={cash_list}
								total_name='Итого доходов:'
								totalSumVal={this.props.cashSum}
							/>

							<AddItemBtn
								type='cash'
								BtnName='доходов'
								modalHandler={this.handlerModal}/>
						</div>

						<div className={styles.moneyDelta}>
							<label className='usdCurrencyField'>
								<span className='_f-s18'>Курс долл США:</span>
								<input
									className='usdCurrencyInp'
									type='text'
									placeholder='введите курс'
								/>
							</label>

							<div className={styles.mainDelta}>
								<h3>Дельта по основному расчету:</h3>
								<p className='_f-s18'>
									<span className={styles.deltaSum}/> руб.
								</p>
								<p className='_f-s18'>
									<span className='deltaSumUSD'/> $
								</p>
							</div>

							<div className={styles.intermediateDelta}>
								<h3>Дельта по промежуточному расчету:</h3>
								<p className='_f-s18'>
									<span className='deltaSumRub'/> руб.
								</p>
								<p className='_f-s18'>
									<span className='deltaSumUSD'/> $
								</p>
							</div>
						</div>
					</div>
				</div>

				<PaymentModal
					open={this.state.modalOpen}
					type={this.state.currentModalType}
					mainPayments={this.props.mainPayments}
					secondPayments={this.props.secondPayments}
					handlerCloseModal={this.handlerModal}
					cash={this.props.cashList}/>
			</div>
		);
	}
}
