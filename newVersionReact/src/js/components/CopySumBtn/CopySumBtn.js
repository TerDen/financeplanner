import React from 'react';
import PropTypes from 'prop-types';
import styles from './CopySumBtn.module.scss';

const CopySumBtn = props => {
	return (
		<button
			onClick={props.onClickCopyBtn}
			className={`${styles.copyPayments} btn btn-success _margin-top-20`}>Копировать
			<i className="fa fa-arrow-right _padding-left-10" aria-hidden="true"/>
		</button>
	);
};

CopySumBtn.propTypes = {
	onClickCopyBtn: PropTypes.func.isRequired
};

export default CopySumBtn;
