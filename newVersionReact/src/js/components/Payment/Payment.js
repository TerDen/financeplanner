import React, {Component} from 'react';
import PropTypes from 'prop-types';

import ItemDescription from '../ItemDescription/ItemDescription';
import DeleteItemBtn from '../DeleteItemBtn/DeleteItemBtn';
import styles from './Payment.module.scss';

export default class Payment extends Component {
	constructor(props) {
		super(props);

		this.sumChangeHandler = this.sumChangeHandler.bind(this);
		this.deleteItemBtnHandler = this.deleteItemBtnHandler.bind(this);
	}

	sumChangeHandler(e) {
		this.props.onChangePaymentSum(
			e.target.id,
			e.target.value,
			this.type.attributes[1].nodeValue
		);
	}

	deleteItemBtnHandler() {
		this.props.deleteItem(this.id.id, this.type.attributes[1].nodeValue);
	}

	render() {
		const transparent = {
				color: 'transparent'
			},
			visible = {
				color: '#000'
			},
			{itemType, text, id, amount, description} = this.props.data;

		return (
			<li
				className={`${styles.item} _margin-top-20`}
				data-belong={itemType}
				ref={type => {
					this.type = type;
				}}
			>
				<input type='checkbox' className={styles.selectPayment}/>
				<div className='_displayFlex'>
					<label>
						<span className={styles.name} datatype='paymentName'>{text}</span>
						<div className='_displayFlex'>
							<input
								id={id}
								ref={id => {
									this.id = id;
								}}
								onChange={this.sumChangeHandler}
								className='_margin-right-20 _halfWidth'
								style={amount > 0 ? visible : transparent}
								type='number'
								placeholder='сумма, руб'
								value={amount === null ? '' : amount}
								data-payment={itemType}
							/>
							<div className={`${styles.savedSum} _halfWidth`}>
								<span
									data-type='savedSum'
									style={amount > 0 ? visible : transparent}
								>
									{amount}
								</span>
								<span className={amount > 0 ? '' : 'hidden'}> руб.</span>
							</div>
						</div>
					</label>
					<DeleteItemBtn deleteItem={this.deleteItemBtnHandler}/>
				</div>
				<ItemDescription
					id={id + '-desc'}
					type={itemType}
					onClickBtnSave={this.props.onClickBtnSave}
					description={description}
				/>
			</li>
		);
	}
}

Payment.propTypes = {
	onChangePaymentSum: PropTypes.func,
	deleteItem: PropTypes.func,
	onClickBtnSave: PropTypes.func,
	data: PropTypes.object,
	type: PropTypes.string,
	text: PropTypes.string,
	id: PropTypes.string,
	amount: PropTypes.number,
	description: PropTypes.string
};
