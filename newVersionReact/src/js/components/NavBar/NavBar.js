import React from 'react';
import PropTypes from 'prop-types';
import NavBarItem from '../NavBarItem/NavBarItem';

import styles from './NavBar.module.scss';

const NavBar = ({monthsData, year}) => {
	return (
		<div className={styles.NavBar}>
			<h2 className={styles.year}>{year}</h2>
			<ul className={styles.list}>
				{monthsData.map((item, i) => (
					<NavBarItem name={item.name} current={item.status} key={i}/>
				))}
			</ul>
		</div>
	);
};

NavBar.propTypes = {
	monthsData: PropTypes.array,
	year: PropTypes.number
};

export default NavBar;
