import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {FontAwesomeIcon} from '@fortawesome/react-fontawesome';

import styles from './ItemDescription.module.scss';

export default class ItemDescription extends Component {
	constructor(props) {
		super(props);

		this.state = {
			visible: false,
			edit: false
		};

		this.handlerShowText = this.handlerShowText.bind(this);
		this.handlerBtnEdit = this.handlerBtnEdit.bind(this);
		this.handlerBtnSave = this.handlerBtnSave.bind(this);
	}

	handlerShowText() {
		this.setState({
			visible: !this.state.visible
		});
	}

	handlerBtnEdit() {
		this.setState({
			edit: !this.state.edit
		});
	}

	handlerBtnSave(e) {
		const id = e.target.id,
			newId = id.slice(0, id.length - 5),
			newDescription = {
				desc: this.textInput.value,
				type: this.type.attributes[0].nodeValue
			};

		this.props.onClickBtnSave(newDescription, newId);
		this.setState({
			edit: !this.state.edit
		});
	}

	render() {
		const {description, id, type} = this.props,
			{visible} = this.state,
			html = this.state.edit ? (
				<div>
					<textarea
						ref={input => {
							this.textInput = input;
						}}
						defaultValue={description}
						placeholder='Введите описание...'
					/>
					<div onClick={this.handlerBtnSave} className='_margin-top-20'>
						<span id={id} className='btn btn-success'>
              Сохранить
						</span>
					</div>
				</div>
			) : (
				<div>
					<p>{description}</p>
					<span
						onClick={this.handlerBtnEdit}
						className='btn btn-success _margin-top-20'
					>
            Редактировать
					</span>
				</div>
			);

		return (
			<div>
				<div
					data-belong={type}
					ref={type => {
						this.type = type;
					}}
				>
					<span
						onClick={this.handlerShowText}
						className='btn btn-default btn-xs'
					>
            Подробнее
						<span className={styles.arrow}>
							<FontAwesomeIcon icon={visible ? 'arrow-up' : 'arrow-down'}/>
						</span>
					</span>
				</div>
				<div
					className={
						visible
							? `${styles.paymentDesc} animated fadeIn _margin-top-20`
							: `${styles.paymentDesc} hidden _margin-top-20`
					}
				>
					<div>{html}</div>
				</div>
			</div>
		);
	}
}

ItemDescription.propTypes = {
	description: PropTypes.string,
	id: PropTypes.string,
	type: PropTypes.string
};
