import React from 'react';
import PropTypes from 'prop-types';
import styles from './AddPaymentModal.module.scss';

const AddPaymentModal = props => {
	const {
		type, obligatory, newPaymentName,
		open, handlerCloseModal, handlerCheckboxChange,
		handlerNewPaymentNameChange, handlerBtnAddCash, handlerBtnAddPayment
	} = props;
	return (
		<div
			id='AddPayment_modal'
			className={`${styles.modal} ${open ? '' : styles.fade}`}
			tabIndex='-1'
			role='dialog'
		>
			<div className='modal-dialog' role='document'>
				<div className='modal-content'>
					<div className='modal-header'>
						<button
							className={styles.closeBtn}
							type='button'
							onClick={handlerCloseModal}>
							<span aria-hidden='true'>&times;</span>
						</button>
						<h4 className='modal-title'>
							Новая статья {type === 'cash' ? 'доходов' : 'расходов'}
						</h4>
					</div>
					<div className={`${styles.body} modal-body`}>
						<label htmlFor='obligatory' className={styles.obligatoryLabel}>
							<input
								id='obligatory'
								onChange={handlerCheckboxChange}
								checked={obligatory}
								type='checkbox'
							/>
							<span className='_padding-left-20'>Обязательная статья</span>
						</label>

						<input
							id='newPaymentName'
							type='text'
							placeholder='Введите название статьи'
							onChange={handlerNewPaymentNameChange}
							value={newPaymentName}
						/>
					</div>
					<div className='modal-footer'>
						<button
							type='button'
							className='btn btn-default'
							data-dismiss='modal'
							onClick={handlerCloseModal}
						>
							Закрыть
						</button>
						<button
							type='button'
							onClick={
								type === 'cash'
									? handlerBtnAddCash
									: handlerBtnAddPayment
							}
							className='btn btn-primary js-addPaymentSuccess'
							data-dismiss='modal'
						>
							Добавить
						</button>
					</div>
				</div>
			</div>
		</div>
	);
};


AddPaymentModal.propTypes = {
	type: PropTypes.string,
	obligatory: PropTypes.bool,
	open: PropTypes.bool,
	newPaymentName: PropTypes.string,
	handlerCloseModal: PropTypes.func,
	handlerCheckboxChange: PropTypes.func,
	handlerNewPaymentNameChange: PropTypes.func,
	handlerBtnAddCash: PropTypes.func,
	handlerBtnAddPayment: PropTypes.func
};

export default AddPaymentModal;
