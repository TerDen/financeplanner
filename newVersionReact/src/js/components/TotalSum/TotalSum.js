import React from 'react';
import PropTypes from 'prop-types';
import styles from './TotalSum.module.scss';

const TotalSum = ({total_name, totalSumVal}) => {
	return (
		<div className={styles.totalSum}>
			<span>{total_name}</span>
			<span className='totalSum'> {totalSumVal}</span>
			<span> руб.</span>
		</div>
	);
};

TotalSum.propTypes = {
	total_name: PropTypes.string,
	totalSumVal: PropTypes.number
};

export default TotalSum;
