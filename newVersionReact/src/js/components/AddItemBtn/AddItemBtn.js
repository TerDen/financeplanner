import React from 'react';
import PropTypes from 'prop-types';

const AddItemBtn = ({type, BtnName, modalHandler}) => {
	return (
		<button
			type='button'
			data-modaltype={type}
			className='addPayment btn btn-primary _margin-top-20'
			onClick={modalHandler}
		>
			Добавить статью {BtnName}
		</button>
	);
};

AddItemBtn.propTypes = {
	type: PropTypes.string.isRequired,
	BtnName: PropTypes.string.isRequired,
	modalHandler: PropTypes.func.isRequired
};

export default AddItemBtn;
