import React from 'react';
import styles from './NavBarItem.module.scss';

const NavBarItem = ({name, current}, key) => {
	return (
		<li
			className={current ? `${styles.item} ${styles.curMonth}` : styles.item}
			key={key}
		>
			<span className='Nav_bar_item-text'>{name}</span>
		</li>
	);
};

export default NavBarItem;
