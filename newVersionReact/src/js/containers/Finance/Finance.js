import React, {Component} from 'react';
import PropTypes from 'prop-types';
import {connect} from 'react-redux';

import {library} from '@fortawesome/fontawesome-svg-core';
import {
	faTrash,
	faArrowDown,
	faArrowUp,
	faCalendarAlt
} from '@fortawesome/free-solid-svg-icons';

import * as A from '../../actions/index';

import Month from '../../components/Month/Month';
import NavBar from '../../components/NavBar/NavBar';

import styles from './Finance.module.scss';

library.add(faTrash, faArrowDown, faArrowUp, faCalendarAlt);

class Finance extends Component {
	render () {
		const {
			calculateTotal,
			handlerSaveItemDescBtn,
			handlerBtnDeleteItem,
			handlerEditItemSum,
			handlerClickCopyBtn,
			mainPayments,
			secondPayments,
			cashList,
			mainSum,
			secondSum,
			cashSum,
			year,
			monthsData
		} = this.props;
		return (
			<div className={styles.Finance}>
				<NavBar monthsData={monthsData} year={year}/>
				<Month
					calculateTotal={calculateTotal}
					handlerSaveItemDescBtn={handlerSaveItemDescBtn}
					handlerBtnDeleteItem={handlerBtnDeleteItem}
					handlerClickCopyBtn={handlerClickCopyBtn}
					handlerEditItemSum={handlerEditItemSum}
					mainPayments={mainPayments}
					secondPayments={secondPayments}
					cashList={cashList}
					mainSum={mainSum}
					secondSum={secondSum}
					cashSum={cashSum}
				/>
			</div>
		);
	}
}

Finance.propeTypes = {
	calculateTotal: PropTypes.func,
	handlerSaveItemDescBtn: PropTypes.func,
	handlerBtnDeleteItem: PropTypes.func,
	handlerEditItemSum: PropTypes.func,
	handlerClickCopyBtn: PropTypes.func,
	mainPayments: PropTypes.object,
	secondPayments: PropTypes.object,
	cashList: PropTypes.object,
	mainSum: PropTypes.number,
	secondSum: PropTypes.number,
	cashSum: PropTypes.number,
	year: PropTypes.number,
	monthsData: PropTypes.object
};

const mapStateToProps = state => {
	return {
		year: state.nav.year,
		monthsData: state.nav.monthsData,
		mainPayments: state.main.list,
		mainSum: state.main.sum,
		secondPayments: state.second.list,
		secondSum: state.second.sum,
		cashList: state.cash.list,
		cashSum: state.cash.sum
	};
};

const mapDispatchToProps = dispatch => {
	return {
		handlerEditItemSum: (id, sum, type) => {
			const payload = {
				id,
				sum,
				type
			};

			dispatch(A.editItemSum(payload));
		},
		calculateTotal: () => {
			dispatch(A.calculateTotal());
		},
		handlerClickCopyBtn: newPayments => {
			dispatch(A.copyPayments(newPayments));
		},
		handlerSaveItemDescBtn: (newDesc, id) => {
			const payload = {
				id,
				paymentDesc: newDesc
			};

			dispatch(A.saveItemDesc(payload));
		},
		handlerBtnDeleteItem: (id, type) => {
			const payload = {
				id,
				type
			};

			dispatch(A.deleteItem(payload));
		}
	};
};

export default connect(mapStateToProps, mapDispatchToProps)(Finance);
