import React, {Component} from 'react';
import {connect} from 'react-redux';
import PropTypes from 'prop-types';
import {v4} from 'uuid';

import AddPaymentModal from '../../components/AddPaymentModal/AddPaymentModal';
import * as A from '../../actions/index';

class PaymentModal extends Component {
	constructor (props) {
		super(props);

		this.state = {
			newPaymentName: '',
			obligatory: true
		};

		this.handlerNewPaymentNameChange = this.handlerNewPaymentNameChange.bind(
			this
		);
		this.handlerBtnAddPayment = this.handlerBtnAddPayment.bind(this);
		this.handlerBtnAddCash = this.handlerBtnAddCash.bind(this);
		this.handlerCheckboxChange = this.handlerCheckboxChange.bind(this);
		this.handlerCloseModal = this.handlerCloseModal.bind(this);
	}

	handlerNewPaymentNameChange (e) {
		this.setState({
			newPaymentName: e.target.value
		});
	}

	handlerCheckboxChange () {
		this.setState({
			obligatory: !this.state.obligatory
		});
	}

	handlerBtnAddPayment (event) {
		const {newPaymentName, obligatory} = this.state;
		const {
			handlerBtnAddPayment_main,
			handlerBtnAddPayment_second
		} = this.props;
		let newPayment = {
			id: v4(),
			text: newPaymentName,
			amount: null,
			itemType: 'mainPayment',
			obligatory,
			description: ''
		};
		handlerBtnAddPayment_main(newPayment);

		newPayment = {
			id: v4(),
			text: newPaymentName,
			amount: null,
			itemType: 'secondPayment',
			obligatory,
			description: ''
		};
		handlerBtnAddPayment_second(newPayment);

		this.setState({
			newPaymentName: ''
		});
		this.props.handlerCloseModal(event);
	}

	handlerBtnAddCash (event) {
		const newCash = {
			id: v4(),
			text: this.state.newPaymentName,
			amount: null,
			itemType: 'mainPayment',
			description: '',
			receive_date: {
				main: '',
				second: ''
			}
		};

		this.props.handlerBtnAddCash(newCash);
		this.props.handlerCloseModal(event);
	}

	handlerCloseModal () {
		this.props.handlerCloseModal();
	}

	render () {
		const {obligatory, newPaymentName} = this.state;
		return (
			<div>
				<AddPaymentModal
					{...this.props}
					obligatory={obligatory}
					newPaymentName={newPaymentName}
					handlerCheckboxChange={this.handlerCheckboxChange}
					handlerNewPaymentNameChange={this.handlerNewPaymentNameChange}
					handlerBtnAddCash={this.handlerBtnAddCash}
					handlerBtnAddPayment={this.handlerBtnAddPayment}/>
			</div>
		);
	}
}

PaymentModal.propTypes = {
	type: PropTypes.string,
	obligatory: PropTypes.bool,
	open: PropTypes.bool,
	newPaymentName: PropTypes.string,
	handlerCloseModal: PropTypes.func
};


export default connect(
	state => ({
		mainPayments: state.main.list,
		secondPayments: state.second.list,
		cash: state.cash.list
	}),
	dispatch => ({
		handlerBtnAddPayment_main: newPaymentObj => {
			dispatch(A.addPaymentMain(newPaymentObj));
		},
		handlerBtnAddPayment_second: newPaymentObj => {
			dispatch(A.addPaymentSecond(newPaymentObj));
		},
		handlerBtnAddCash: newCashObj => {
			dispatch(A.addCash(newCashObj));
		}
	})
)(PaymentModal);
