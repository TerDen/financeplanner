import React from 'react';
import cx from 'classnames';

import './Colorpicker.less';

const COLORS = ['#fff', '#80D8FF', '#ffff8D', '#ff8a80', '#ccff90', '#cfd8dc', '#ffd180'];

export default class ColorPicker extends React.Component {
  render() {
    return (
      <div className='ColorPicker'>
        {
          COLORS.map(color =>
            <div
              key={color}
              className={cx('ColorPicker__swatch', {selected: this.props.value === color })}
              style={{backgroundColor: color}}
              onClick={this.props.onChange.bind(null, color)}
            />
          )
        }
      </div>
    )
  }
}