import React from 'react';

import './App.less';
import NotesStore from '../../stores/NotesStore';
import NotesActions from '../../actions/NotesActions';

import NotesEditor from '../NotesEditor/NotesEditor';
import NotesGrid from '../NotesGrid/NotesGrid';

function getStateFromFlux() {
  return {
    isLoading: NotesStore.isLoading(),
    notes: NotesStore.getNotes()
  }
}

export default class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = getStateFromFlux();

    this.handleNoteAdd = this.handleNoteAdd.bind(this);
    this.handleNoteDelete = this.handleNoteDelete.bind(this);
    this._onChange = this._onChange.bind(this);
  }

  componentWillMount() {
    NotesActions.loadNotes();
  }

  componentDidMount() {
    NotesStore.addChangeListener(this._onChange);
  }

  componentWillUnmount() {
    NotesStore.removeChangeListener(this._onChange);
  }

  handleNoteAdd(note) {
    NotesActions.createNote(note);
  }

  handleNoteDelete(note) {
    NotesActions.deleteNote(note.id);
  }

  _onChange() {
    const state = getStateFromFlux();
    this.setState(state);
  }

  render() {
    return (
      <div className='App'>
        <h1 className='App__header'>Notes app</h1>
        <NotesEditor onNoteAdd={this.handleNoteAdd}/>
        <NotesGrid notes={this.state.notes} onNoteDelete={this.handleNoteDelete}/>
      </div>
    )
  }
}