import React from 'react';

import './NotesEditor.less';
import ColorPicker from '../Colorpicker/Colorpicker';

export default class NotesEditor extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '',
      text: '',
      color: '#fff'
    };

    this.handleTextChange = this.handleTextChange.bind(this);
    this.handleTitleChange = this.handleTitleChange.bind(this);
    this.handleNoteAdd = this.handleNoteAdd.bind(this);
    this.handleColorChange = this.handleColorChange.bind(this);
  }

  handleTextChange(e) {
    this.setState({
      text: e.target.value
    })
  };

  handleTitleChange(e) {
    this.setState({
      title: e.target.value
    })
  };

  handleNoteAdd() {
    const newNote = {
      title: this.state.title,
      text: this.state.text,
      color: this.state.color
    };

    this.props.onNoteAdd(newNote);
    this.setState({
      title: '',
      text: '',
      color: '#fff'
    });
  }

  handleColorChange(color) {
    this.setState({color});
  }

  render() {
    return (
      <div className='NoteEditor'>
        <input
          type='text'
          placeholder='Enter title'
          className='NoteEditor__title'
          value={this.state.title}
          onChange={this.handleTitleChange}
        />
        <textarea
          placeholder='Enter note text'
          rows={5}
          className='NoteEditor__text'
          value={this.state.text}
          onChange={this.handleTextChange}
        />
        <div
          className='NoteEditor__footer'>

          <ColorPicker
            value={this.props.color}
            onChange={this.handleColorChange}
          />

          <button
            className='NoteEditor__button'
            disabled={!this.state.text}
            onClick={this.handleNoteAdd}
          >Add</button>
        </div>
      </div>
    )
  }
}