var config = {
  entry: "./client/main.js",
  devtool: 'source-map',
  output: {
    path: __dirname + '/public/build/',
    publicPath: "build/",
    filename: "bundle.js"
  },
  module: {
    loaders: [
      {
        test: /\.js$/,
        exclude: [/node_modules/, /public/],
        loader: "babel-loader",
        query: {
          presets: ['react', 'es2015']
        }
      },
      {
        test: /\.less$/,
        loader: ["style-loader", "css-loader", "autoprefixer-loader", "less-loader"],
        exclude: [/node_modules/, /public/]
      },
      {
        test: /\.jsx$/,
        loader: ['react-hot-loader', 'babel'],
        exclude: [/node_modules/, /public/]
      },
      {
        test: /\.json$/,
        loader: "json-loader"
      }
    ]
  }
};

module.exports = config;